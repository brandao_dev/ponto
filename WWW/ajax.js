$(function()
{
    $('#frase').html(
        "<div class='alert alert-warning animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-calendar-alt mr-2'></i>Controle Mensal</strong></div>"
    )

    let dat = new Date();

    //VALOR DO BOTÃO
    let atual = dat.getMonth()+'/'+dat.getFullYear()
    $('#atual').val(atual)

    //SE ATUAL. BLOQUEIA PROXIMO
    if($('#atual').val() == atual)
    {
        $('#prox').prop('disabled', true)
    }
    else
    {
        $('#prox').prop('disabled', false)
    }

    //TEXTO DO BOTÃO
    let ano = ['JANEIRO','FEVEREIRO','MARÇO','ABRIL','MAIO','JUNHO','JULHO','AGOSTO','SETEMBRO','OUTUBRO','NOVEMBRO','DEZEMBRO']
    $('#atual').html(ano[atual.split('/')[0]]+' / '+atual.split('/')[1])

    mes()

})

function mes(mes)
{
    let botao = $('#atual').val()
    let month =  parseInt(botao.split('/')[0])+1 
    let date = new Date(botao.split('/')[1]+','+month+','+1)
    let ano = ['JANEIRO','FEVEREIRO','MARÇO','ABRIL','MAIO','JUNHO','JULHO','AGOSTO','SETEMBRO','OUTUBRO','NOVEMBRO','DEZEMBRO']

    if(mes == 0)
    {
        //TEXTO DO BOTÃO
        date.setMonth(date.getMonth()-1)
        $('#atual').html(ano[date.getMonth()]+' / '+date.getFullYear())

        //VALOR DO BOTÃO
        $('#atual').val(date.getMonth()+'/'+date.getFullYear())
        classe = 'animate__animated animate__lightSpeedInLeft'
        novo = date.getMonth()+'/'+date.getFullYear()
    }
    else
    if(mes == 1)
    {
        //TEXTO DO BOTÃO
        date.setMonth(date.getMonth()+1)
        $('#atual').html(ano[date.getMonth()]+' / '+date.getFullYear())

        //VALOR DO BOTÃO
        $('#atual').val(date.getMonth()+'/'+date.getFullYear())
        classe = 'animate__animated animate__lightSpeedInRight'
        novo = date.getMonth()+'/'+date.getFullYear()
    }
    else
    {
        novo = botao
        classe = ''
    }
    $.ajax(
        {
            url: 'mes.php',
            data: {mes:novo,classe:classe},
            type: 'POST',
            dataType: 'json',
            success: function(dados)
            {
                $('#mes').empty()
                $('#mes').append(dados)

                //SE ATUAL. BLOQUEIA PROXIMO

                let dat = new Date();
                let atual = dat.getMonth()+'/'+dat.getFullYear()
                if($('#atual').val() == atual)
                {
                    $('#prox').prop('disabled', true)
                }
                else
                {
                    $('#prox').prop('disabled', false)
                }
            },
            error: function()
            {
                console.log('error ')
            }
        }
    )
}

function filtro(dia)
{
    let botao = $('#atual').val()
    let d =  dia+'/'+(parseInt(botao.split('/')[0])+1)+'/'+botao.split('/')[1]

    $.ajax(
        {
            data: 'dia='+d,
            url: 'dia.php',
            type: 'POST',
            dataType: 'JSON',
            success: (r)=>
            {
                console.log(r)
                let div = ''

                $.each(r, function(i,v)
                {
                    if(v[0] != false)
                    {
                        div += '<label class="mt-3">'+i+'</label>'+
                                '<div class="progress" style="height: 30px;">'+
                                    '<div class="progress-bar bg-secondary" role="progressbar" style="width:'+parseInt(v[1])+'%" aria-valuemin="0" aria-valuemax="100"><strong>'+parseInt(v[1])+'%</strong></div>'+
                                    '<div class="progress-bar bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width:'+(100-parseInt(v[1])-parseInt(v[2]))+'%" aria-valuemin="0" aria-valuemax="100"><strong>'+(100-parseInt(v[1])-parseInt(v[2]))+'%</strong></div>'+
                                    '<div class="progress-bar bg-secondary" role="progressbar" style="width:'+parseInt(v[2])+'%" aria-valuemin="0" aria-valuemax="100"><strong>'+parseInt(v[2])+'%</strong></div>'+
                                '</div>'
                    }
                    else
                    {
                        div += '<label class="mt-3">'+i+'</label>'+
                                '<div class="progress">'+
                                    '<div class="progress-bar bg-secondary" role="progressbar" style="width:100%" aria-valuemin="0" aria-valuemax="100">AUSENTE</div>'+
                                '</div>'
                    }
                })

                swal.fire(
                    {
                        title: 'Dia '+d,
                        html:'<div class="text-left">'+div+'</div>',
                        customClass: 'swal',
                        showConfirmButton: false
                    }
                )
            }
        }
    )
}
