<?php 
	include('../CORE/HEADER.php');
	include('../CSS/css.php');
?>

<script src="ajax.js?<?php echo filemtime('ajax.js')?>"></script>
<script src="../AGGRID/luz.js?<?php echo filemtime('../AGGRID/luz.js')?>"></script>

<div class="row row animate__animated text-center animate__fadeInDown mt-3">
    <div class="col-12 col-lg-5 form-group text-left">
        <button class="btn btn-primary shadow" onclick="mes(0),vibrate(100),sonora('click')"><i class="fas fa-arrow-circle-left mr-2"></i>Anterior</button>
        <button class="btn btn-outline-primary font-weight-bold" disabled id="atual"></button>
        <button class="btn btn-primary shadow" id="prox" onclick="mes(1),vibrate(100),sonora('click')">Próximo<i class="fas fa-arrow-circle-right ml-2"></i></button>
    </div>
    <div class="col-12 col-lg-7 form-group">
        <span title="Menos ou mais de 4 registros"><i class="far text-primary fa-alarm-exclamation ml-2 mr-2"></i>Registro</span>
        <span title="Entrada atrasada"><i class="far text-orange fa-alarm-clock ml-2 mr-2"></i>Atraso</span>
        <span title="Saída antecipada"><i class="far text-danger fa-alarm-snooze ml-2 mr-2"></i>Antecipação</span>
        <span title="Menos de 8 horas no dia"><i class="far text-success fa-alarm-plus ml-2 mr-2"></i>Jornada</span>
        <span title="Funcionário ausente"><i class="far text-secondary fa-alarm-plus ml-2 mr-2"></i>Ausência</span>
    </div>
</div>
<div class="row animate__animated text-center animate__fadeInDown" id='mes'>
</div> 

<?php include('../CORE/FOOTER.php')?>