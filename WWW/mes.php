<?php include('../CORE/CONN.php');

$data = $_POST['mes'];
$classe = $_POST['classe'];
$mes = explode('/',$data)[0];
$ano = explode('/',$data)[1];

$busca = substr('0'.strval($mes+1),-2).'/'.$ano;
// $agenda = conex()->query("SELECT agenda FROM ordens WHERE agenda LIKE '%$busca'")->fetchAll(PDO::FETCH_ASSOC);

// $base = conex()->query("SELECT * FROM base WHERE data LIKE '%$busca'")->fetchAll(PDO::FETCH_ASSOC);

//LISTA TODOS FUNCIONARIOS
$nomes = conex()->query("SELECT * FROM funcionarios")->fetchAll(PDO::FETCH_ASSOC);

//ARRAY UNIQUE MULTIDIMENSIONAL
$dias = array_map('unserialize',array_unique(array_map('serialize',conex()->query("SELECT data FROM base WHERE data LIKE '%$busca'")->fetchAll(PDO::FETCH_ASSOC))));

//LISTA TODOS REGISTROS
// foreach($base as $b)
// {
    //PRA CADA DIA ENM CADA REGISTRO
    foreach($dias as $d)
    {
        $dia = explode('/',$d['data'])[0].'/'.$busca;

        foreach($nomes as $n)
        {
            //PRA CADA FUNCIONARIO ORECISA DE 4 REGISTROS POR DIA, SENÂO MARCA ALERTA
            $registros = array_map('unserialize',array_map('serialize',conex()->query("SELECT hora FROM base WHERE data = '$dia' AND nome = '{$n['nome']}'")->fetchAll(PDO::FETCH_ASSOC)));
            //SO VAI agendaR ENTRADA OU SAIDA FORA DO HORARIO SE HOUVER 4 REGISTROS
            if(count($registros) == 4)
            {
                //SE ENTROU ATRASADA
                if(strtotime($registros[0]['hora']) > strtotime($n['entrada']))
                {
                    $agenda[] = $dia.'.<i class="far text-orange fa-alarm-clock" title="'.$n['nome'].'"></i>';
                }

                //SE SAIU MAIS CEDO
                if(strtotime($registros[3]['hora']) < strtotime($n['saida']))
                {
                    $agenda[] = $dia.'.<i class="far text-danger fa-alarm-snooze" title="'.$n['nome'].'"></i>';
                }
    
                //SE CARGA DIARIA NÂO COMPLETOU 8 HORAS
                // $carga = (strtotime($registros[1]['hora'])-strtotime($registros[0]['hora']))+(strtotime($registros[3]['hora'])-strtotime($registros[2]['hora']));

                $entrada = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$registros[0]['hora'].':00');
                $saida = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$registros[3]['hora'].':00');

                $diff_trabalhada = $entrada->diff($saida);
                $horas_totais = ($diff_trabalhada->h * 60) + $diff_trabalhada->i; //quantidade de minutos entre a entrada e saída do funcionário na empresa

                $saida_intervalo = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$registros[1]['hora'].':00');
                $volta_intervalo = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$registros[2]['hora'].':00');

                $diff_intervalo = $saida_intervalo->diff($volta_intervalo);
                $horas_intervalo = ($diff_intervalo->h * 60) + $diff_intervalo->i; //quantidade de minutos do intervalo do funcionário

                //subtraindo essas horas totais do intervalo temos a quantidade de horas trabalhadas
                $horas_trabalhadas = $horas_totais - $horas_intervalo;
                $horas = (int)($horas_trabalhadas/60); //horas inteiras trabalhadas
                $minutos = $horas_trabalhadas % 60; //minutos trabalhados
                // echo "{$horas}:{$minutos}"; //concatenação entre os dois        //OBRIGATORIAS //////////////////////////////////////////////////////////////////////////////////////////////

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                $entrada_o = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$n['entrada']);
                $saida_o = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$n['saida']);
                $dif_o = $entrada_o->diff($saida_o);
                $periodo_bruto = ($dif_o->h * 60)+ $dif_o->i;
                $periodo_o =  $periodo_bruto - $n['intervalo']; //PERIODO OBRIGATORIO EM MINUTOS
    
                if($horas_trabalhadas < $periodo_o)
                {
                    $agenda[] = $dia.'.<i class="far text-success fa-alarm-plus" title="'.$n['nome'].'"></i>';
                }
            }
            elseif(count($registros) == 0)
            {
                $agenda[] = $dia.'.<i class="far text-secondary fa-alarm-exclamation" title="'.$n['nome'].'"></i>';
            }
            else
            {
                $agenda[] = $dia.'.<i class="far text-primary fa-alarm-exclamation" title="'.$n['nome'].'"></i>';
            }
        }  
    }  
// }


// print_r($alerta);

$calendario = calendario($mes, $agenda, $classe, $ano);


function MostreSemanas()
{
    $semana = array('DOM','SEG','TER','QUA','QUI','SEX','SAB');
    $d = null;
	for( $i = 0; $i < 7; $i++ )
	$d .= "<th class='border-primary'>".$semana[$i]."</th>";
    return $d;
}

function GetNumeroDias( $mes, $ano )
{
	$numero_dias = array( 
			'0' => 31, '1' => 28, '2' => 31, '3' =>30, '4' => 31, '5' => 30,
			'6' => 31, '7' =>31, '8' => 30, '9' => 31, '10' => 30, '11' => 31
	);

	if ((($ano % 4) == 0 and ($ano % 100)!=0) or ($ano % 400)==0)
	{
	    $numero_dias['1'] = 29;	// altera o numero de dias de fevereiro se o ano for bissexto
	}

	return $numero_dias[$mes];
}

function calendario($mes, $agenda, $classe, $ano)
{
    $div = '';
    
    $numero_dias = GetNumeroDias( $mes, $ano );	// retorna o número de dias que tem o mês desejado
    $diacorrente = 0;	
    
    $diasemana = jddayofweek(cal_to_jd(CAL_GREGORIAN, $mes+1,"01",$ano) , 0 );
    $div = "<div class='col-12 ".$classe."'><table class='table table-bordered'><thead style='background-color: #B4C6E7;'><tr>";
    $div .= MostreSemanas();	// função que mostra as semanas aqui
    $div .= "</tr></thead><tbody>";
    
    for( $linha = 0; $linha <= 5; $linha++ )
    {
        $div .= "<tr>";
    
        for( $coluna = 0; $coluna < 7; $coluna++ )
        {
            if( $diacorrente + 1 <= $numero_dias )
            {
                if( $coluna < $diasemana && $linha == 0)
                {
                $d = " ";
                $bg = '';
                }
                else
                {
                $d = ++$diacorrente;
                $bg = 'bg-white';
                }
                $div .= "<td class='border-primary font-weight-bold ".$bg."' onclick='filtro(".$d.")'><p>".$d."</p>".css($d,$agenda)."</td>";;
            }
            else
            {
                break;
            }
        }
        $div .= "</tr>";
    }
    $div .= "</tbody></table></div>";

    return $div;
}

function css($d,$agenda)
{
    $icone = '';
    foreach($agenda as $a)
    {
        if(explode('.',explode('/',$a)[0])[0] == $d)
        {
            $icone .= explode('.',$a)[1];
        }
    }

    return implode(',',array_unique(explode(',',$icone)));
}

print json_encode($calendario);

?>