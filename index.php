<!DOCTYPE html>
<html lang="pt-br">
	<head>

		<!-- HEAD 4============================================================-->
		<?php include('CORE/HEAD.php')?>
		<!-- ================================================================= -->

		<!-- PWA ==============================================================-->
		<link rel="manifest" href="manifest.json?<?php echo filemtime("manifest.json")?>"/>
		<script type="module" src="pwa.js?<?php echo filemtime('pwa.js')?>"></script>
		<script src="APP/share.js?<?php echo filemtime('APP/share.js')?>"></script>
		<meta name="theme-color" content="#dae3f3">
		<!-- ================================================================= -->

		<!-- MAIN SCRIPT====================================================== -->
		<?php include('CSS/css.php')?>
		<script src="LOGIN/AJAX.js?<?php echo filemtime('LOGIN/AJAX.js')?>"></script>
		<style>
			body, html {
			height: 100%;
			background: #DAE3F3;  
			}
			.bg {

			/* Full height */
			height: 100%;
			/* Center and scale the image nicely */
			background-position: center;
			background-repeat: no-repeat;
			background-size: cover;
			}
			footer { 
			position:absolute;
			bottom:0;
			z-index: 3;
			}
		</style>
		<!-- ================================================================== -->
		<script>
			sessionStorage.setItem('corpo', 'animate__backInRight')
		</script>
		
	</head>
		<body class="animate__animated text-center corpo animate__backInLeft">

		<!-- CORPO -->
		<div class="container-fluid  text-center d-flex justify-content-center">
			<div class="row mt-5">
				<div class="col-12 mb-3">
					<img src="IMG/P.png" width="200px" alt="Ponto" title="Sistema de Leitura de Ponto de funcionários">
				</div>
				<div class="col-12 mt-5">
				<form action="" method="POST" id="login">
					<div class="card border-cyan" id="quadro">
						<div class="card-body" style="background-color: #DAE3F3;">

							<!-- NOME -->
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text border-right-0 bg-white"><i class="fas fa-user"></i></span>
								</div>
								<input class="form-control bg-white font-weight-bold border-left-0" placeholder="Login" type="text" name="nome" id="nome" title="Login do usuário">
							</div>

							<!-- SENHA -->
							<div class="input-group mt-3">
								<div class="input-group-prepend">
									<span class="input-group-text border-right-0 bg-white"><i class="fas fa-key"></i></span>
								</div>
								<input class="form-control bg-white font-weight-bold border-left-0" placeholder="Senha" type="password" name="senha" id="senha" title="Senha do usuário">
							</div>

							<!-- ERRO -->
							<div class="col text-center mb-3 mt-3 text-danger font-weight-bold" id="erro"></div>

							<button class="btn btn-lg btn-block btn-primary shadow mb-3" type="submit" id="botao" onclick="vibrate(100)">
								<i class="fas fa-door-open mr-2"></i>Entrar
							</button>

							<button type="button" class="add-button btn btn-lg btn-block btn-info shadow" onclick="vibrate(100), sonora('install')"><i class="fas fa-download mr-2"></i>Instalar App</button>

							<div class="card-footer text-secondary text-center mt-5">
								<a href="#" class="text-cyan" style="text-decoration: none;" onclick="share()" ><i class="fas fa-share-alt mr-2"></i>Compartilhar</a>
							</div>
						</div>
					</div>
				</form>

				</div>
			</div>
			<br>
			<footer class="mt-auto font-weight-bold text-secondary">
			<p>®Ponto, by <a href="https://brandev.site" target="_blank">Br@ndeveloper</a> 2021 - V 1.5</p>
			</footer>
		</div>
	</body>
</html>