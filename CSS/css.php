
<!-- AUDIO =============================================================
=====================================================================-->
<audio id="alert" src="../SOUND/alert/1.mp3"></audio>
<audio id="error" src="../SOUND/error/1.wav"></audio>
<audio id="success" src="../SOUND/ok/1.mp3"></audio>
<audio id="efect" src="../SOUND/efect/efect.wav"></audio>
<audio id="click" src="../SOUND/efect/click.wav"></audio>
<audio id="total" src="../SOUND/alert/3.wav"></audio>
<audio id="queda" src="../SOUND/error/3.wav"></audio>
<audio id="install" src="../SOUND/ok/3.mp3"></audio>


<!-- TAMANHO DE SWAL====================================================
==================================================================== -->
<style>
.swal {width:800px !important;}
.swalG {width:1000px !important;}
.mini {width:300px !important;}
.audio {height:200px !important;}
</style>


<!-- PISCANDO ==========================================================
=====================================================================-->
<style>
	 @keyframes fa-blink {
     0% { opacity: 1; }
     50% { opacity: 0.5; }
     100% { opacity: 0; }
 }
.fa-blink {
   -webkit-animation: fa-blink .99s linear infinite;
   -moz-animation: fa-blink .99s linear infinite;
   -ms-animation: fa-blink .99s linear infinite;
   -o-animation: fa-blink .99s linear infinite;
   animation: fa-blink .99s linear infinite;
}
</style>

<!-- BADGE REDONDO =================================================
=================================================================-->
<style>
.item {
    position:relative;
}
.notify-badge{
    position: absolute;
    right:10px;
    top:-20px;
    text-align: center;
    border-radius: 30px 30px 30px 30px;
    padding:5px 18px;
    font-size:40px;
}

</style>

<!-- SOM E VIBRAR ===================================================
==================================================================-->
<script>
//  .d8888b.                         
// d88P  Y88b                        
// Y88b.                             
//  "Y888b.    .d88b.  88888b.d88b.  
//     "Y88b. d88""88b 888 "888 "88b 
//       "888 888  888 888  888  888 
// Y88b  d88P Y88..88P 888  888  888 
//  "Y8888P"   "Y88P"  888  888  888

function sonora(tip) {
	document.getElementById(tip).play();
  } 

// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888  

function vibrate(ms){
navigator.vibrate(ms);
}    

// 8888888888P                        
//       d88P                         
//      d88P                          
//     d88P     .d88b.  88888b.d88b.  
//    d88P     d88""88b 888 "888 "88b 
//   d88P      888  888 888  888  888 
//  d88P       Y88..88P 888  888  888 
// d8888888888  "Y88P"  888  888  888   

function zom(id)
{
    play()
    Swal.fire({
        html: '<img src="'+id+'" width="100%" class="rounded shadow">',
        customClass: 'image',
        backdrop: 'rgba(0,0,0,0.9)',
        showConfirmButton: false,
      })
}
</script>

