
$(function() 
{
    $('#login').on('submit',function(e)
    {
        e.preventDefault();
        var con = $(this).serialize();
        $.ajax(
        {
            type: 'POST',
            dataType: 'json',
            url: 'LOGIN/PROC.php',
            async: true,
            data: con,
            success: function(confirma)
            {
                $('#erro').html(confirma.erro);
                if(confirma.logon != false)
                {
                    $('.corpo').removeClass('animate__backInLeft')
                    $('.corpo').addClass('animate__backOutLeft')

                    setTimeout(() => {
                        window.location='../'+confirma.logon
                    }, 500);
                }
                else
                {
                    sonora('error')
                }
            }
        }
            )
        
    }
    );
}
);

// MANDA TELEGRAM
function telegram(user)
{
    if(user.length == 0)
    {
        Swal.fire({
            title: "Por favor",
            text: "Escreva seu nome para que posssamos entrar em contato!",
            icon: "info",
            backdrop: `rgba(50,50,50,1)`
        }   
        )
    }
    else
    {
        var msg = "Eu, "+user+", não lembro da minha senha";
        var token = '1068499951:AAHC4Xoz-GXa24HejM1LZb2aIARPU5_mArc';
        //SMART LOGIN CHANNEL
        var chat = '-1001268762199';
        async function file_get_contents(uri, callback) {
            let res = await fetch(uri),
                ret = await res.text(); 
            return callback ? callback(ret) : ret; // a Promise() actually.
        }
        file_get_contents("https://api.telegram.org/bot"+token+"/sendMessage?chat_id="+chat+"&text="+msg);

    }
}

//  .d8888b.                         
// d88P  Y88b                        
// Y88b.                             
//  "Y888b.    .d88b.  88888b.d88b.  
//     "Y88b. d88""88b 888 "888 "88b 
//       "888 888  888 888  888  888 
// Y88b  d88P Y88..88P 888  888  888 
//  "Y8888P"   "Y88P"  888  888  888

function sonora(tip) {
	document.getElementById(tip).play();
  } 

// 888     888 d8b 888                               
// 888     888 Y8P 888                               
// 888     888     888                               
// Y88b   d88P 888 88888b.  888d888  8888b.  888d888 
//  Y88b d88P  888 888 "88b 888P"       "88b 888P"   
//   Y88o88P   888 888  888 888     .d888888 888     
//    Y888P    888 888 d88P 888     888  888 888     
//     Y8P     888 88888P"  888     "Y888888 888  

function vibrate(ms){
navigator.vibrate(ms);
}   
