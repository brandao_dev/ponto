<?php include('../CORE/CONN.php');

$dia = substr('0'.$_POST['dia'],-10);

$pessoas = conex()->query("SELECT nome, entrada, saida, intervalo FROM funcionarios")->fetchAll(PDO::FETCH_ASSOC);

foreach($pessoas as $p)
{
    $registros = conex()->query("SELECT hora FROM base WHERE nome = '{$p['nome']}' AND data = '$dia'")->fetchAll(PDO::FETCH_ASSOC);

    if(count($registros) == 4)
    {
        //OBRIGATORIAS //////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $entrada_o = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$p['entrada']);
        $saida_o = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$p['saida']);
        $dif_o = $entrada_o->diff($saida_o);
        $periodo_bruto = ($dif_o->h * 60)+ $dif_o->i;
        $periodo_o =  $periodo_bruto - $p['intervalo']; //PERIODO OBRIGATORIO EM MINUTOS

        //********************************************************************************************************* */
        
        //REGISTRADAS //////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $entrada = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$registros[0]['hora'].':00');
        $saida = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$registros[3]['hora'].':00');

        $diff_trabalhada = $entrada->diff($saida);
        $horas_totais = ($diff_trabalhada->h * 60) + $diff_trabalhada->i; //quantidade de minutos entre a entrada e saída do funcionário na empresa

        $saida_intervalo = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$registros[1]['hora'].':00');
        $volta_intervalo = new DateTime(explode('/',$dia)[2].'/'.explode('/',$dia)[1].'/'.explode('/',$dia)[0].' '.$registros[2]['hora'].':00');

        $diff_intervalo = $saida_intervalo->diff($volta_intervalo);
        $horas_intervalo = ($diff_intervalo->h * 60) + $diff_intervalo->i; //quantidade de minutos do intervalo do funcionário

        //subtraindo essas horas totais do intervalo temos a quantidade de horas trabalhadas
        $horas_trabalhadas = $horas_totais - $horas_intervalo; //TRABALHADO EM MINUTOS
        $horas = (int)($horas_trabalhadas/60); //horas inteiras trabalhadas
        $minutos = $horas_trabalhadas % 60; //minutos trabalhados
        // echo "{$horas}:{$minutos}"; //concatenação entre os dois
        $jorna = $horas_trabalhadas*100/$periodo_o;

        // echo $p['nome'].$horas_trabalhadas.'<br>';
    
        $atr[$p['nome']][] = (int)$jorna;
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //          d8888 888                                       
        //         d88888 888                                       
        //        d88P888 888                                       
        //       d88P 888 888888 888d888  8888b.  .d8888b   .d88b.  
        //      d88P  888 888    888P"       "88b 88K      d88""88b 
        //     d88P   888 888    888     .d888888 "Y8888b. 888  888 
        //    d8888888888 Y88b.  888     888  888      X88 Y88..88P 
        //   d88P     888  "Y888 888     "Y888888  88888P'  "Y88P"  

        if(strtotime($registros[0]['hora']) > strtotime($p['entrada']))
        {
            //CALCULAR DIFERENÇA ENTRE ENTRADA REGISTRADA E ENTRADA OBRIGATORIA
            $atraso = $entrada_o->diff($entrada);
            $atraso_minutos = ($atraso->h * 60) + $atraso->i;

            //RELAÇÂO PERCENTUAL
            $relacao_e = $atraso_minutos*100/$periodo_o;

            $atr[$p['nome']][] = (int)$relacao_e;
        }
        else
        {
            $atr[$p['nome']][] = 0;
        }


        //  .d8888b.           d8b      888          
        // d88P  Y88b          Y8P      888          
        // Y88b.                        888          
        //  "Y888b.    8888b.  888  .d88888  8888b.  
        //     "Y88b.     "88b 888 d88" 888     "88b 
        //       "888 .d888888 888 888  888 .d888888 
        // Y88b  d88P 888  888 888 Y88b 888 888  888 
        //  "Y8888P"  "Y888888 888  "Y88888 "Y888888 

        if(strtotime($registros[3]['hora']) < strtotime($p['saida']))
        {
            //CALCULAR DIFERENÇA ENTRE SAIDA REGISTRADA E SAIDA OBRIGATORIA
            $antecipada = $saida_o->diff($saida);
            $antecipa_minutos = ($antecipada->h * 60) + $antecipada->i;

            // RELAÇÂO PERCENTUAL
            $relacao_s = $antecipa_minutos*100/$periodo_o;

            $atr[$p['nome']][] = (int)$relacao_s;

        }  
        else
        {
            $atr[$p['nome']][] = 0;
        }                                                        

    }
    else
    {
        $atr[$p['nome']][] = false; 
    }
}

print json_encode($atr);

?>