$(function()
{
    $('#frase').html(
        "<div class='alert alert-warning animate__animated animate__flipInX barra'><strong class='mr-5'><i class='far fa-calendar-check mr-2'></i>Resumo Mensal</strong></div>"
    )

    let dat = new Date();

    //VALOR DO BOTÃO
    let atual = dat.getMonth()+'/'+dat.getFullYear()
    $('#atual').val(atual)

    console.log(atual)
    //SE ATUAL. BLOQUEIA PROXIMO
    if($('#atual').val() == atual)
    {
        $('#prox').prop('disabled', true)
    }
    else
    {
        $('#prox').prop('disabled', false)
    }

    //TEXTO DO BOTÃO
    let ano = ['JANEIRO','FEVEREIRO','MARÇO','ABRIL','MAIO','JUNHO','JULHO','AGOSTO','SETEMBRO','OUTUBRO','NOVEMBRO','DEZEMBRO']
    $('#atual').html(ano[atual.split('/')[0]]+' / '+atual.split('/')[1])

    mes()

})

function mes(mes)
{
    let botao = $('#atual').val()
    let month =  parseInt(botao.split('/')[0])+1 
    let date = new Date(botao.split('/')[1]+','+month+','+1)
    let ano = ['JANEIRO','FEVEREIRO','MARÇO','ABRIL','MAIO','JUNHO','JULHO','AGOSTO','SETEMBRO','OUTUBRO','NOVEMBRO','DEZEMBRO']

    if(mes == 0)
    {
        //TEXTO DO BOTÃO
        date.setMonth(date.getMonth()-1)
        $('#atual').html(ano[date.getMonth()]+' / '+date.getFullYear())

        //VALOR DO BOTÃO
        $('#atual').val(date.getMonth()+'/'+date.getFullYear())
        classe = 'animate__animated animate__lightSpeedInLeft'
        novo = date.getMonth()+'/'+date.getFullYear()
    }
    else
    if(mes == 1)
    {
        //TEXTO DO BOTÃO
        date.setMonth(date.getMonth()+1)
        $('#atual').html(ano[date.getMonth()]+' / '+date.getFullYear())

        //VALOR DO BOTÃO
        $('#atual').val(date.getMonth()+'/'+date.getFullYear())
        classe = 'animate__animated animate__lightSpeedInRight'
        novo = date.getMonth()+'/'+date.getFullYear()
    }
    else
    {
        novo = botao
        classe = ''
    }
    $.ajax(
        {
            url: 'resumo.php',
            data: {mes:novo,classe:classe},
            type: 'POST',
            dataType: 'json',
            success: function(dados)
            {
                $('#mes').empty()
                $('#mes').append(dados)

                //SE ATUAL. BLOQUEIA PROXIMO

                let dat = new Date();
                let atual = dat.getMonth()+'/'+dat.getFullYear()
                if($('#atual').val() == atual)
                {
                    $('#prox').prop('disabled', true)
                }
                else
                {
                    $('#prox').prop('disabled', false)
                }
            },
            error: function()
            {
                console.log('error ')
            }
        }
    )
}