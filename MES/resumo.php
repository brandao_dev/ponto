<?php include('../CORE/CONN.php');

$data = substr('0'.$_POST['mes'],-7);

$classe = $_POST['classe'];

$mes = explode('/',$data)[0];
// $PR = substr('0'.strval($mes+1),-2);
// echo $PR;
// $DC = substr($PR,-2);
$ano = explode('/',$data)[1];

$dias = GetNumeroDias($mes,$ano);

function GetNumeroDias($mes, $ano)
{
	$numero_dias = array( 
			'00' => 31, '01' => 28, '02' => 31, '03' =>30, '04' => 31, '05' => 30,
			'06' => 31, '07' =>31, '08' => 30, '09' => 31, '10' => 30, '11' => 31
	);

	if ((($ano % 4) == 0 and ($ano % 100)!=0) or ($ano % 400)==0)
	{
	    $numero_dias['1'] = 29;	// altera o numero de dias de fevereiro se o ano for bissexto
	}

	return $numero_dias[$mes];
}

$pessoas = conex()->query("SELECT nome, entrada, saida, intervalo, folga FROM funcionarios")->fetchAll(PDO::FETCH_ASSOC);

//PARA CADA PESSOA
foreach($pessoas as $p)
{

    //OBRIGATORIAS //////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $entrada_o = new DateTime('2020/01/01 '.$p['entrada']);
    $saida_o = new DateTime('2020/01/01 '.$p['saida']);
    $dif_o = $entrada_o->diff($saida_o);
    $periodo_bruto = ($dif_o->h * 60)+ $dif_o->i;
    $periodo =  $periodo_bruto - $p['intervalo'];
    $total_obrigatorio = $periodo * ($dias - $p['folga']);

    $total_trabalhado = 0;

    //LEITURA PARA CADA DIA DO MES
    for($d = 1; $d <= $dias; $d++)
    {
        //CONCATENA DATA
        $dat = substr('0'.$d,-2).'/'.substr('0'.strval($mes+1),-2).'/'.$ano;
        // echo $dat;

        $query = "SELECT hora FROM base WHERE nome = '{$p['nome']}' AND data = '$dat'";
        if(conex()->query($query)->fetchAll(PDO::FETCH_ASSOC))
        {
            // echo $p['nome'].' - '.$dat.'</br>';
            $reg = conex()->query($query)->fetchAll(PDO::FETCH_ASSOC);
    
            if(count($reg) == 4)
            {

                //REGISTRADAS //////////////////////////////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                $entrada = new DateTime(explode('/',$dat)[2].'/'.explode('/',$dat)[1].'/'.explode('/',$dat)[0].' '.$reg[0]['hora'].':00');
                $saida = new DateTime(explode('/',$dat)[2].'/'.explode('/',$dat)[1].'/'.explode('/',$dat)[0].' '.$reg[3]['hora'].':00');
    
                $diff_trabalhada = $entrada->diff($saida);
                $horas_totais = ($diff_trabalhada->h * 60) + $diff_trabalhada->i; //quantidade de minutos entre a entrada e saída do funcionário na empresa
    
                $saida_intervalo = new DateTime(explode('/',$dat)[2].'/'.explode('/',$dat)[1].'/'.explode('/',$dat)[0].' '.$reg[1]['hora'].':00');
                $volta_intervalo = new DateTime(explode('/',$dat)[2].'/'.explode('/',$dat)[1].'/'.explode('/',$dat)[0].' '.$reg[2]['hora'].':00');
    
                $diff_intervalo = $saida_intervalo->diff($volta_intervalo);
                $horas_intervalo = ($diff_intervalo->h * 60) + $diff_intervalo->i; //quantidade de minutos do intervalo do funcionário
    
                //subtraindo essas horas totais do intervalo temos a quantidade de horas trabalhadas
                $total_trabalhado += $horas_totais - $horas_intervalo; //TRABALHADO EM MINUTOS
                
            }
        }
    }

    $r[] = ['nome'=>$p['nome'],'obri'=>$total_obrigatorio,'trab'=>$total_trabalhado];

}

$nomes = null;

foreach($r as $div)
{
    $bar = $div['trab']*100/$div['obri'];
    $res = 100-$bar;
    $fal = $div['obri']-$div['trab'];
    $nomes .= '
    <div class="col-12 text-left '.$classe.'">
    <label>'.$div['nome'].'</label>
        <div class="progress" style="height: 20px;">
            <div class="progress-bar bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width:'.$bar.'%" aria-valuemin="0" aria-valuemax="100">
                <strong>'.(int)($div['trab']/60).' hs</strong>
            </div>
            <div class="progress-bar bg-secondary" role="progressbar" style="width:'.$res.'%" aria-valuemin="0" aria-valuemax="100">
                <strong>'.(int)($fal/60).' hs</strong>
            </div>
        </div>
    </div>';
}

print json_encode($nomes);

?>