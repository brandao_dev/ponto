<?php 
	include('../CORE/HEADER.php');
	include('../CSS/css.php');
?>

<script src="ajax.js?<?php echo filemtime('ajax.js')?>"></script>

<div class="row row animate__animated text-center animate__fadeInDown mt-3">
    <div class="col-12 col-lg-5 form-group text-left">
        <button class="btn btn-primary shadow" onclick="mes(0),vibrate(100),sonora('click')"><i class="fas fa-arrow-circle-left mr-2"></i>Anterior</button>
        <button class="btn btn-outline-primary font-weight-bold" disabled id="atual"></button>
        <button class="btn btn-primary shadow" id="prox" onclick="mes(1),vibrate(100),sonora('click')">Próximo<i class="fas fa-arrow-circle-right ml-2"></i></button>
    </div>
</div>
<div class="row mt-3 row text-center">
    <div class="col-12">
        <div class="card shadow bg-light">
            <div class="card-body">
                <div class="row" id='mes'></div>
            </div>
        </div>
    </div>
</div> 

<?php include('../CORE/FOOTER.php')?>