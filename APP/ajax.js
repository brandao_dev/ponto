$(()=>
{
    
    $("#file").on("change", function() 
    {
        var fileName = $(this).val().split("\\").pop();
        $("#frase").html("<div class='alert alert-success animate__animated animate__flipInX'><strong class='ml-5'>Arquivo: "+fileName+"</strong></div>");
        sonora('efect')
        $('#sub').prop('disabled', false)
        $('#sub').removeClass('btn-secondary')
        $('#sub').addClass('btn-primary')
    });

    $('#processa').on('submit', function(event)
    {
        event.preventDefault();

        $.ajax(
            {
                url: 'processa.php',
                data: new FormData($(this)[0]),
                type: 'POST',
                dataType: 'json',
                contentType: false,
                processData: false,
                success: (i)=>
                {
                    $('#frase').html(i[0])
                    sonora(i[1])

                    // LIMPAR INPUT
                    $('#file').val('')

                    //BLOQUEAR CARREGA
                    $('#sub').prop('disabled', true)
                    $('#sub').removeClass('btn-primary')
                    $('#sub').addClass('btn-secondary')

                    if(i[2] == true)
                    {
                        atualiza()
                    }
                }                
            }
        )

    })
})