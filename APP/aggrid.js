// COLUNAS //////////////////////////////////////////
var columnDefs = 
[
  {headerName: "NSR", field: "nsr", filter: "agNumberColumnFilter",comparator: (valueA, valueB, nodeA, nodeB, isInverted) => valueA - valueB},
  {headerName: "DATA", field: "data", filter: "agDateColumnFilter"},
  {headerName: "HORA", field: "hora", filter: "agTextColumnFilter"},
  {headerName: "NOME", field: "nome", filter: "agSetColumnFilter"}
];