<?php

include_once("../CORE/CONN.php");

//Receber os dados do formulário
if(!empty($_FILES['file']['tmp_name']))
{
	$arquivo_tmp = $_FILES['file']['tmp_name'];
	
	//ler todo o arquivo para um array
	$dados = file($arquivo_tmp);	
	
	$ultimo_registro = conex()->query("SELECT nsr FROM base ORDER BY nsr DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC)['nsr'];

	$_SESSION['msg'] = '<div class="alert alert-danger animate__animated animate__flipInX text-center"><strong class="mr-5"><i class="fas fa-bug mr-2"></i>Arquivo corrompido!</strong></div>';
	$alert = 'error';
	$n = 0;
	$atualiza = false;
	
	//Ler os dados do array
	foreach($dados as $linha)
	{
		//IGNORA O LINHA QUE NÃO TENHA 38 CASAS
		if(strlen($linha) == 40)
		{
			$nsr = substr($linha,0,9);
	
			//IGNORA LINHAS JA REGISTRADAS
			if($nsr > $ultimo_registro)
			{
				$data = substr($linha,10,2).'/'.substr($linha,12,2).'/'.substr($linha,14,4);
				$hora = substr($linha,18,2).':'.substr($linha,20,2);
				$pis = substr($linha,22,12);
	
				if($nome = conex()->query("SELECT nome FROM funcionarios WHERE pis = '$pis'")->fetch(PDO::FETCH_ASSOC))
				{
					$nome = $nome['nome'];
	
					//GRAVAR
					conex()->prepare("INSERT INTO base (data,nome,nsr,hora) VALUES ('$data', '$nome', '$nsr', '$hora')")->execute();
					$n++;
	
					$_SESSION['msg'] = "<div class='alert alert-success animate__animated animate__flipInX text-center'><strong class='mr-5'><i class='fas fa-glass-cheers mr-2'></i>Parabéns! ".$n." registros carregados com sucesso!</strong></div>";
					$alert = 'install';
					$atualiza = true;
				}
			}
			else
			{
				$_SESSION['msg'] = '<div class="alert alert-danger animate__animated animate__flipInX text-center"><strong class="mr-5"><i class="fas fa-exclamation-triangle mr-2"></i>Nenhum registro novo para ser importado!</strong></div>';
				$alert = 'error';
			}
		}
	}
}
else
{
	$_SESSION['msg'] = 	'<div class="alert alert-danger animate__animated animate__flipInX text-center"><strong class="ml-5"><i class="fas fa-exclamation-triangle mr-2"></i>Nada para importar!</strong></div>';
	$alert = 'error';
}

$retorno[] = $_SESSION['msg'];
$retorno[] = $alert;
$retorno[] = $atualiza;

print json_encode($retorno);