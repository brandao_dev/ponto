function share() {
    if (!("share" in navigator)) {
      alert('Web Share API not supported.');
      return;
    }
  
    navigator.share({
        title: 'HMS Home Clean',
        text: 'Compartilhar App HMS Home Clean',
        url: 'https://hms.brandev.site'
      })
      .then(() => console.log('Successful share'))
      .catch(error => console.log('Error sharing:', error));
  }