<?php 
	include('../CORE/HEADER.php');
	include('../AGGRID/aggrid.php');
	include('../CSS/css.php');

	//Imprimir a mensagem de sucesso no upload de dados do arquivo txt para o banco de dados com mysqli
	if(isset($_SESSION['msg'])){
		//Imprimir o valor da sessão
		$ultimo = $_SESSION['msg'];
		//Destruir a sessão com PHP
		unset($_SESSION['msg']);
	}
	if(conex()->query("SELECT data FROM base order by nsr DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC))
	{
		$u = conex()->query("SELECT data FROM base order by nsr DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC)['data'];
	}
	else
	{
		$u = 'VAZIO';
	}
?>
	<script src="ajax.js?<?php echo filemtime('ajax.js')?>"></script>
	
	<div class="row">

		<div class="col-12" id="celular">
			<form method="POST" id="processa" enctype="multipart/form-data">
				<label for="file" class="btn btn-primary shadow mt-2" onclick="sonora('click'),vibrate(100)" title="Buscar arquivo do pendrive">
					<i class="fas fa-search mr-2"></i>Procurar
				</label>
				<input type="file" id="file" name="file"  style="display:none" accept=".txt"/>
				
				<button type="submit" class="btn btn-secondary shadow" id="sub" disabled onclick="vibrate(100)" title="Subir arquivo para o Banco de Dados">
					<i class="fas fa-upload mr-2"></i>Carregar
				</button>
				<button class="btn btn-warning" disabled><?php echo 'O último registro é de '.$u?></button>
			</form>
		</div>
	</div>
	
	<div id="myGrid" style="position:absolute; width: 98%;height:75%" class="ag-theme-alpine"></div>

<?php include('../CORE/FOOTER.php')?>