//FORMATA PARA MOEDA
function format(params) {
	const formatter = new Intl.NumberFormat('pt-br', {
		style: 'currency',
		currency: 'BRL',
		minimumFractionDigits: 2
	  })
  return formatter.format(params.value/100);
}

//FUNCTION GRUPO
function grupo(param)
{
	if(param.value == 'R'){$grupo = 'REFRIGERAÇÃO'}else if(param.value == 'C'){$grupo = 'COCÇÃO'}else if(param.value == 'L'){$grupo = 'LAVANDERIA'}else{$grupo = 'OUTROS'}
	return $grupo
}

//FUNÇÃO PERCENTUAL
function percent(param)
{
	return param.value+' %'
}