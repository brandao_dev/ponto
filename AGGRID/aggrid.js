// OPÇÔES DA GRID E PAINEL LATERAL ///////////////// ¹²³
var gridOptions = 
{
	statusBar: 
	{
        statusPanels: [
            { statusPanel: 'agTotalAndFilteredRowCountComponent', align: 'left' },
            { statusPanel: 'agFilteredRowCountComponent' },
            { statusPanel: 'agSelectedRowCountComponent' },
            { statusPanel: 'agAggregationComponent' }
        ]
    },
	defaultColDef: 
	{
		menuTabs: [],
		resizable: true,
		sortable: true,
		floatingFilter: true,
		filterParams: 
		{
			buttons:['reset'],
			inRangeInclusive:true,
			includeBlanksInEquals:false,
			includeBlanksInRange:false,
			includeBlanksANTreaterThan:false,
			includeBlanksInLessThan:false,
			comparator: function (filterLocalDateAtMidnight, cellValue) 
			{
			   var dateAsString = cellValue;
			   if (dateAsString == null) return 0;
			   // In the example application, dates are stored as dd/mm/yyyy
			   // We create a Date object for comparison against the filter date
			   var dateParts = dateAsString.split("\/");
			   var day = Number(dateParts[2]);
			   var month = Number(dateParts[1]) - 1;
			   var year = Number(dateParts[0]);
			   var cellDate = new Date(day, month, year);
			   // Now that both parameters are Date objects, we can compare
			   if (cellDate < filterLocalDateAtMidnight) {return -1;} else if (cellDate > filterLocalDateAtMidnight) {return 1;} else {return 0;}
			}
		}
	},
	columnDefs: columnDefs,
    enableRangeSelection: true,
	suppressCsvExport: true,
	rowSelection: 'multiple',
	onSelectionChanged: getRow,
	animateRows: true,
	getRowNodeId: function (data) {
	  return data.id;
	},
	sideBar: 
	{
		toolPanels: 
		[{
			id: 'columns',
			labelDefault: 'Columns',
			labelKey: 'columns',
			iconKey: 'columns',
			toolPanel: 'agColumnsToolPanel',
			toolPanelParams: 
			{
				suppressPivots: true,
				suppressPivotMode: true,
				suppressValues: true,
				suppressRowGroups: true
			}
		}]
},
// TRADUÇÂO DOS MENUS ////////////////////////////////////////////////////////////////////////////////////////	
	localeText : {
	// for filter panel
	page: 'Página',
	more: 'Mais',
	to: 'Para',
	of: 'A',
	next: 'Proximo',
	last: 'Ultimo',
	first: 'Primeiro',
	previous: 'Anterior',
	loadingOoo: 'Carregando...',

	// for set filter
	selectAll: 'Selecione Tudo',
	searchOoo: 'Procurar...',
	blanks: 'Branco',

	// for number filter and text filter
	filterOoo: 'Filtro...',
	applyFilter: 'Aplicar Filtro...',
	equals: 'Igual',
	notEqual: 'Diferente',

	// for number filter
	lessThan: 'Menor que',
	greaterThan: 'Maior que',
	lessThanOrEqual: 'Menor ou Igual a',
	greaterThanOrEqual: 'Maior ou igual a',
	inRange: 'Entre',

	// for text filter
	contains: 'Contém',
	notContains: 'Nao Contém',
	startsWith: 'Começa com',
	endsWith: 'Termina com',

	// filter conditions
	andCondition: 'E',
	orCondition: 'OU',

	// the header of the default group column
	group: 'Grupo',

	// tool panel
	columns: 'Colunas',
	filters: 'Filtros',
	rowGroupColumns: 'Grupo de Colunas',
	rowGroupColumnsEmptyMessage: 'Arrastar Coluna para Grupo',
	valueColumns: 'Valor da Coluna',
	pivotMode: 'Modo Pivo',
	groups: 'Grupos',
	values: 'Valores',
	pivots: 'Pivos',
	valueColumnsEmptyMessage: 'Arrastar Coluna para Agregar',
	pivotColumnsEmptyMessage: 'Arraste aqui para Pivo',
	toolPanelButton: 'Painel de Ferramentas',

	// other
	noRowsToShow: 'Sem linhas',

	// enterprise menu
	pinColumn: 'Fixar Coluna',
	valueAggregation: 'Valor Agregado',
	autosizeThiscolumn: 'Ajustar esta Coluna',
	autosizeAllColumns: 'Ajustar todas as Colunas',
	groupBy: 'Agrupado por',
	ungroupBy: 'Desagrupado por',
	resetColumns: 'Resetar Colunas',
	expandAll: 'Expandir Tudo',
	collapseAll: 'Colapsar Tudo',
	toolPanel: 'Painel de Ferramentas',
	export: 'Exportar',
	csvExport: 'laCSV Exportp',
	excelExport: 'Exportar Planilha Excel (.XLSX)',
	excelXmlExport: 'Exportar (.XML)',

	// enterprise menu (charts)
	chartRange: 'laChart Range',

	columnRangeChart: 'laColumn',
	groupedColumnChart: 'laGrouped',
	stackedColumnChart: 'laStacked',
	normalizedColumnChart: 'la100% Stacked',

	barRangeChart: 'laBar',
	groupedBarChart: 'laGrouped',
	stackedBarChart: 'laStacked',
	normalizedBarChart: 'la100% Stacked',

	lineRangeChart: 'laLine',

	pieRangeChart: 'laPie',
	pieChart: 'laPie',
	doughnutChart: 'laDoughnut',

	areaRangeChart: 'laArea',
	areaChart: 'laArea',
	stackedAreaChart: 'laStacked',
	normalizedAreaChart: 'la100% Stacked',

	// enterprise menu pinning
	pinLeft: 'Fixar a Esquerda <<',
	pinRight: 'Fixar a Direita >>',
	noPin: 'Desfixar <>',

	// enterprise menu aggregation and status bar
	sum: 'Soma',
	min: 'Menor Valor',
	max: 'Maior Valor',
	none: 'Nada',
	count: 'Contagem',
	average: 'Média',
	filteredRows: 'Linhas Filtradas',
	selectedRows: 'Linhas Selecionadas',
	totalRows: 'Linhas Totais',
	totalAndFilteredRows: 'Linhas Totais Filtradas',

	// standard menu
	copy: 'Copiar',
	copyWithHeaders: 'Copiar com Cabeçalho',
	ctrlC: 'Ctrl + C',
	paste: 'Colar',
	ctrlV: 'Ctrl + V',

	// charts
	settings: 'laSettings',
	data: 'laData',
	format: 'laFormat',
	categories: 'laCategories',
	series: 'laSeries',
	axis: 'laAxis',
	color: 'laColor',
	thickness: 'laThickness',
	xRotation: 'laX Rotation',
	yRotation: 'laY Rotation',
	ticks: 'laTicks',
	width: 'laWidth',
	length: 'laLength',
	padding: 'laPadding',
	chart: 'laChart',
	title: 'laTitle',
	font: 'laFont',
	top: 'laTop',
	right: 'laRight',
	bottom: 'laBottom',
	left: 'laLeft',
	labels: 'laLabels',
	size: 'laSize',
	legend: 'laLegend',
	position: 'laPosition',
	markerSize: 'laMarker Size',
	markerStroke: 'laMarker Stroke',
	markerPadding: 'laMarker Padding',
	itemPaddingX: 'laItem Padding X',
	itemPaddingY: 'laItem Padding Y',
	strokeWidth: 'laStroke Width',
	offset: 'laOffset',
	tooltips: 'laTooltips',
	offsets: 'laOffsets',
	callout: 'laCallout',
	markers: 'laMarkers',
	shadow: 'laShadow',
	blur: 'laBlur',
	xOffset: 'laX Offset',
	yOffset: 'laY Offset',
	lineWidth: 'laLine Width',
	normal: 'laNormal',
	bold: 'laBold',
	italic: 'laItalic',
	boldItalic: 'laBold Italic',
	fillOpacity: 'laFill Opacity',
	strokeOpacity: 'laLine Opacity',
	groupedColumnTooltip: 'laGrouped',
	stackedColumnTooltip: 'laStacked',
	normalizedColumnTooltip: 'la100% Stacked',
	groupedBarTooltip: 'laGrouped',
	stackedBarTooltip: 'laStacked',
	normalizedBarTooltip: 'la100% Stacked',
	pieTooltip: 'laPie',
	doughnutTooltip: 'laDoughnut',
	lineTooltip: 'laLine',
	groupedAreaTooltip: 'laGrouped',
	stackedAreaTooltip: 'laStacked',
	normalizedAreaTooltip: 'la100% Stacked'
}
};

//GET SELECTED ROW
function getRow() 
{	
	let selectedNodes = gridOptions.api.getSelectedNodes();

	if(typeof ativarBotoes === 'function')
	{
		ativarBotoes(selectedNodes.map(node => node.data))
	}

	return selectedNodes.map(node => node.data);
}

// UPDATE ROW
function updateRow(id,obj)
{
	let rowNode = gridOptions.api.getRowNode(id);

	$.each(obj, function(ind,val)
	{
		rowNode.setDataValue(ind,val)
	})
}  

//ADD ROW
function addRow(id,obj)
{
	$.extend(obj,{id:id})
	gridOptions.api.applyTransaction({add:[obj], addIndex:0})
}

//DELETE ROW
function deleteRow(row)
{	
	gridOptions.api.applyTransaction({remove:row});
}

// CARREGAR JSON //////////////////////////////////
document.addEventListener('DOMContentLoaded', function() 
{    
	atualiza()
});

function atualiza()
{
	$('#myGrid').empty();
	var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

    // do http request to get our sample data - not using any framework to keep the example self contained.
    // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
    var httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'get.php');
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
            var httpResult = JSON.parse(httpRequest.responseText);
            gridOptions.api.setRowData(httpResult);
        }
    };

}