function luz(luz)
{
	if(luz == 1)
	{
		$('#myGrid').removeClass('ag-theme-alpine-dark')
		$('#myGrid').addClass('ag-theme-alpine')
		$('#corpo').removeClass('bg-dark')
		$('#on').attr('checked', true)
		$('#off').attr('checked', false)
	}
	else
	{
		$('#myGrid').removeClass('ag-theme-alpine')
		$('#myGrid').addClass('ag-theme-alpine-dark')
		$('#corpo').addClass('bg-dark')
		$('#off').attr('checked', true)
		$('#on').attr('checked', false)
	}
	
	$.ajax(
		{
			url: '../AGGRID/luz.php',
			data: 'luz='+luz,
			type: 'POST'
		}
	)

}