var gulp = require('gulp');
var browsersync = require('browser-sync').create();
var sass = require('gulp-sass')(require('sass'))
var php = require('gulp-connect-php');


//Compilar o Sass
gulp.task('sassSD',gulp.series( function() {
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss', 'SCSS/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest("CSS"))
        .pipe(browsersync.stream());

}));

//mover js para src.js
gulp.task('jsSD',gulp.series( function() {
    return gulp.src(['node_modules/bootstrap/dist/js/bootstrap.js', 'node_modules/jquery/dist/jquery.js', 'node_modules/@popperjs/core/dist/umd/popper.js'])
        .pipe(gulp.dest("JS"))
        .pipe(browsersync.stream());

}));

//servidor para olhar os Html /scss
gulp.task('serverSD', gulp.series( ['sassSD'], function() {
    browsersync.init({
        proxy:"http://localhost:3030",
        https: {
            key: "ssl/server.key",
            cert: "ssl/server.crt"
            },
        baseDir: "./",
        port: 3030

    });
    
    php.server({base:'./', port:3030, keepalive:true});
    gulp.watch(['node_modules/bootstrap/scss/bootstrap.scss', 'SCSS/*.scss'], gulp.parallel( ['sassSD']));
    gulp.watch("./**/*.*").on('change',gulp.parallel( browsersync.reload));

}));

// =====================================================
gulp.task('PONTO', gulp.series( ['jsSD', 'serverSD']));