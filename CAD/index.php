<?php 
	include('../CORE/HEADER.php');
	include('../CSS/css.php');
?>
<script src="../CAD/ajax.js?<?php echo filemtime('../CAD/ajax.js')?>"></script>

<div class="row">
	<div class="col-12">
		<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead class="thead-dark">
				<tr>
					<th><i class='fas fa-user mr-2'></i>Nome</th>
					<th class="d-none d-sm-table-cell"><i class='fas fa-address-card mr-2'></i>PIS</th>
					<th class="d-none d-md-table-cell"><i class="fas fa-user-clock mr-2"></i>Entrada / Saída</th>
					<th class="d-none d-lg-table-cell"><i class="fas fa-user-minus mr-2"></i>Intervalo</th>
					<th class="d-none d-xl-table-cell"><i class="fas fa-user-ninja mr-2"></i>Folgas</th>
					<th class="d-none d-xl-table-cell"><i class="fas fa-user-cog mr-2"></i>Ação</th>
				</tr>
			</thead>
			<tbody id="tbody">
			</tbody>
		</table>
		</div>
	</div>
</div>

<?php include('../CORE/FOOTER.php')?>