<?php include('../CORE/CONN.php');

    $bd = conex()->query("SELECT * FROM funcionarios ORDER BY nome")->fetchAll(PDO::FETCH_ASSOC);
    $ret = null;

    function hora($id, $entrada, $saida)
    {
        $hora = "    
        <div class='input-group'>
            <input type='time' class='form-control' value='$entrada' name='e".$id."' required>
            <input type='time' class='form-control' value='$saida' name='s".$id."' required>
        </div>";
        return $hora;
    }

    foreach($bd as $f)
    {
        $ret .= "
        <tr>
            <td><input type='text' value='{$f['nome']}' name='n{$f['id']}' class='form-control'></td>
            <td class='d-none d-sm-table-cell'><input type='text' value='{$f['pis']}' name='p{$f['id']}' class='form-control'</td>
            <td class='d-none d-md-table-cell'>
                <div class='input-group'>
                    <input type='time' class='form-control' value='{$f['entrada']}' name='e{$f['id']}' required>
                    <input type='time' class='form-control' value='{$f['saida']}' name='s{$f['id']}' required>
                </div>
            </td>
            <td class='d-none d-lg-table-cell'><input type='number' required value='{$f['intervalo']}' name='i{$f['id']}' class='form-control'</td>
            <td class='d-none d-xl-table-cell'><input type='number' required value='{$f['folga']}' name='f{$f['id']}' class='form-control'</td>
            <td class='text-center d-none d-xl-table-cell'>
                <button type='button' onclick='editar(".$f['id'].")' class='btn btn-primary shadow' title='Salvar Alterações'><i class='fas fa-save'></i></button>
                <button type='button' onclick='exclui(".$f['id'].")' class='btn btn-danger shadow' title='Excluir Usuário'><i class='fas fa-trash-alt'></i></button>
            </td>
        </tr>";
    }

    $ret .= "
    
        <tr>
            <td><input type='text' id='nome' placeholder='Nome completo' class='form-control'></td>
            <td class='d-none d-sm-table-cell'><input type='number' id='pis'  placeholder='PIS sem traço' class='form-control'></td>
            <td class='d-none d-md-table-cell'>
                <div class='input-group'>
                    <input type='time' class='form-control' id='entrada' required>
                    <input type='time' class='form-control' id='saida' required>
                </div>
            </td>
            <td class='d-none d-lg-table-cell'><input type='number' required placeholder='Em minutos' id='intervalo' class='form-control'</td>
            <td class='d-none d-xl-table-cell'><input type='number' required placeholder='Em dias' id='folga' class='form-control'</td>
            <td class='text-center d-none d-xl-table-cell'>
                <button type='button' onclick='add()' class='btn btn-success shadow' title='Adicionar usuário'><i class='fas fa-user-plus mr-2'></i>Add</button>
            </td>
        </tr>"; 

    print json_encode($ret);
?>