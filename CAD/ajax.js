$(()=>
{
    $('#frase').html(
        "<div class='alert alert-warning animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-ballot-check mr-2'></i>Relação de funcionários</strong></div>"
    )
    leitura()
})

function leitura()
{
    $.ajax(
        {
            url: '/CAD/leitura.php',
            dataType: 'json',
            success: (i)=>
            {
                $('#tbody').empty()
                $('#tbody').append(i)
            }
        }
    )
}

function editar(id)
{
    let nome = $('input[name="n'+id+'"]').val()
    let pis = $('input[name="p'+id+'"]').val()
    let entrada = $('input[name="e'+id+'"]').val()
    let saida = $('input[name="s'+id+'"]').val()
    let intervalo = $('input[name="i'+id+'"]').val()
    let folga = $('input[name="f'+id+'"]').val()

    $.ajax(
        {
            url: '/CAD/grava.php',
            data: {id:id,nome:nome,pis:pis,entrada:entrada,saida:saida,intervalo:intervalo,folga:folga},
            type: 'POST',
            success: function()
            {
                $('#frase').html(
                    "<div class='alert alert-success animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-user-edit mr-2'></i>Parabéns. Funcionário editato</strong></div>"
                )
                leitura()
                sonora('success')
            }
        }
    )
}

function add()
{
    let nome = $('#nome').val()
    let pis = $('#pis').val()
    let entrada = $('#entrada').val()
    let saida = $('#saida').val()
    let intervalo = $('#intervalo').val()
    let folga = $('#folga').val()

    if(nome.length < 1)
    {
        $('#frase').html(
            "<div class='alert alert-danger animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-exclamation-triangle mr-2'></i>Erro! Insira o nome completo</strong></div>"
        )
        sonora('error')
    }
    
    else
    {
        if(pis.length != 11)
        {
            $('#frase').html(
                "<div class='alert alert-danger animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-exclamation-triangle mr-2'></i>Erro! PIS inválido.</strong></div>"
            )
            sonora('error')
        }
        else
        {
            if(entrada.length < 4)
            {
                $('#frase').html(
                    "<div class='alert alert-danger animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-exclamation-triangle mr-2'></i>Erro! Entrada inválida.</strong></div>"
                )
                sonora('error')
            }
            else
            {
                if(saida.length < 4)
                {
                    $('#frase').html(
                        "<div class='alert alert-danger animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-exclamation-triangle mr-2'></i>Erro! Saída inválida.</strong></div>"
                    )
                    sonora('error')
                }
                else
                {
                    if(intervalo.length == 0)
                    {
                        $('#frase').html(
                            "<div class='alert alert-danger animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-exclamation-triangle mr-2'></i>Erro! Insira pelo menos 0 no intervalo.</strong></div>"
                        )
                        sonora('error')
                    }
                    else
                    {
                        if(folga.length == 0)
                        {
                            $('#frase').html(
                                "<div class='alert alert-danger animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-exclamation-triangle mr-2'></i>Erro! Insira pelo menos 0 na folga.</strong></div>"
                            )
                            sonora('error')
                        }
                        else
                        {
                            $.ajax(
                                {
                                    url: '/CAD/add.php',
                                    type: 'POST',
                                    data: {nome:nome,pis:pis,entrada:entrada,saida:saida,intervalo:intervalo,folga:folga},
                                    success: function()
                                    {
                                        $('#frase').html(
                                            "<div class='alert alert-success animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-user-plus mr-2'></i>Funcionário adicionado com sucesso!</strong></div>"
                                        )
                                        leitura()
                                        sonora('install')
                                    }
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}

function exclui(id)
{
    $.ajax(
        {
            data: 'id='+id,
            url: '/CAD/excluir.php',
            type: 'POST',
            success: function()
            {
                $('#frase').html(
                    "<div class='alert alert-danger animate__animated animate__flipInX barra'><strong class='mr-5'><i class='fas fa-user-slash mr-2'></i>Funcionário excluído com sucesso!</strong></div>"
                )
                leitura()
                sonora('success')
            }
        }
    )
}