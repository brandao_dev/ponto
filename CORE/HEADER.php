<?php
include "../CORE/CONN.php";
include('../CORE/REDIRECT.php');
?>

<!DOCTYPE HTML>
<html lang='pt-br'>
<head>

	<!-- HEAD ======================================================================================== -->
	<?php include('../CORE/HEAD.php')?>
	<!-- ============================================================================================= -->


	<style>
		.big-checkbox {width: 30px; height: 30px;}
		body.swal2-shown > [aria-hidden="true"] { transition: 1s ; filter: blur(3px) grayscale(0.7);}
		html {
			height: 100%;
			min-height: 100%;
		}

		body {
			display: flex;
			flex-direction: column;
			min-height: 100%;
		}

		footer {
			margin-top: auto;
		}
	</style>
</head>
<body style="background-color: #DAE3F3;" class="animate__animated" id="corpo">

	<!-- MARGEM DO CORPO -->
	<div class='container-fluid'>
	<nav class="navbar navbar-expand-sm bg-secondary navbar-dark">
			<!-- <i class="fas fa-2x text-white fa-chevron-circle-left btn" id="volta" onclick="vibrate(100)" title="Voltar"></i> -->

			<img src="../IMG/P.png" alt="ponto" width="100px" title="Sistema de Leitura de Ponto de funcionários" class="d-inline-block align-top navbar-brand ml-1">

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#dv" aria-controls="dv" aria-expanded="false" aria-label="Alterna navegação">
				<span class="navbar-toggler-icon"></span>
  			</button>

			<div class="collapse navbar-collapse" id="dv">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item active" id="APP">
						<a class="nav-link btn proximo" href="/APP/" onclick="vibrate(100)" title="Upload de arquivo do ponto"><i class="fas fa-scroll mr-2"></i>Coleta</a>
					</li>
					<li class="nav-item" id="CAD">
						<a class="btn nav-link proximo" href="/CAD/" onclick="vibrate(100)" title="Cadastro de Funcionário"><i class="fas fa-users-cog mr-2"></i>Cadastro</a>
					</li>
					<li class="nav-item" id="MES">
						<a class="btn nav-link proximo" href="/MES/" onclick="vibrate(100)" title="Horas acumuladas"><i class="far fa-calendar-check mr-2"></i>Acumulado</a>
					</li>
					<li class="nav-item" id="WWW">
						<a class="btn nav-link proximo" href="/WWW/" onclick="vibrate(100)" title="Grafico de acompanhamento"><i class="fas fa-chart-bar mr-2"></i>Resumo</a>
					</li>
				</ul>
			</div>
			<!-- <i class="fas fa-2x text-white fa-chevron-circle-right btn" id="frente" onclick="vibrate(100)" title="Avançar"></i> -->

		</nav>
