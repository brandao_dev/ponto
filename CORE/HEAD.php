    <title>ponto</title>
        <!-- COMMOM META ====================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="../IMG/F.png"/>
        <meta name="description" content="Author: Brandeveloper, Category: Service App, Length: 2 pages">
        <!-- ================================================================== -->

        <!-- Add to homescreen for Chrome on Android ==========================-->
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="application-name" content="Ponto">
        <!-- ================================================================= -->

        <!-- Add to homescreen for Safari on iOS ==============================-->
        <meta name="apple-mobile-web-app-status-bar-style" content="white">
        <meta name="apple-mobile-web-app-title" content="Ponto">
        <link rel="apple-touch-icon" href="IMG/192.png">
        <!-- ================================================================= -->

        <!-- Tile for Win8 ====================================================-->
        <meta name="msapplication-TileColor" content="#343A40">
        <meta name="msapplication-TileImage" content="IMG/192.png">
        <!-- ================================================================= -->

        <!-- OG FACEBOOK ===================================================== -->
        <meta property="og:locale" content="pt-br"/>
        <meta property="og:url" content="https://ponto.brandev.site/"/>
        <meta property="og:title" content="Ponto"/>
        <meta property="og:site_name" content="Ponto"/>
        <meta property="og:description" content="Ponto"/>
        <meta property="og:image" content="https://ponto.brandev.site/IMG/400x300.png"/>
        <meta property="og:image:type" content="image/png"/>
        <meta property="og:image:width" content="300"/>
        <meta property="og:image:height" content="400"/>
        <meta property="og:type" content="product"/>
        <!-- ================================================================= -->

        <!-- TWITTER ========================================================= -->
        <meta name="twitter:url" content="https://ponto.brandev.site/">
        <meta name="twitter:title" content="Ponto">
        <meta name="twitter:description" content="Ponto">
        <meta name="twitter:image" content="https://ponto.brandev.site/IMG/400x300.png">
        <meta charset="UTF-8">
        <!-- ================================================================= -->

        <!-- BRANDEV CDN =====================================================-->
        <link rel="stylesheet" href="../CSS/custom.css">
        <link href='https://cdn.brandev.site/css/all.css' rel='stylesheet'>
	    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <!--==================================================================-->

        <!-- EXTRA CDN ====================================================== -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>	
        <!-- =================================================================-->

        <!-- USER ============================================================-->
        <?php echo isset($_SESSION['user']) ? "<script> var user = '{$_SESSION['user']}';</script>" : ''?>
        <!-- ================================================================ -->