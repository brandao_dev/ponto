﻿

		<!-- LOGO -->
		<nav class="navbar navbar-expand-sm bg-secondary navbar-dark" style="display: none;">
			<i class="fas fa-2x text-white fa-chevron-circle-left btn" id="volta" onclick="vibrate(100), sonora('click')" title="Voltar"></i>

			<img src="../IMG/P.png" alt="ponto" width="100px" title="Sistema de Leitura de Ponto de funcionários" class="d-inline-block align-top navbar-brand ml-1">

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#dv" aria-controls="dv" aria-expanded="false" aria-label="Alterna navegação">
				<span class="navbar-toggler-icon"></span>
  			</button>

			<div class="collapse navbar-collapse" id="dv">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
					<li class="nav-item active">
						<button class="nav-link btn" id="home" title="Upload de arquivo do ponto"><i class="fas fa-scroll mr-2"></i>Coleta</button>
					</li>
					<li class="nav-item">
						<button class="btn nav-link" id="resumo" title="Grafico de acompanhamento"><i class="fas fa-chart-bar mr-2"></i>Resumo</button>
					</li>
				</ul>
			</div>

			<a href="javascript:history.forward()" onclick="vibrate(100), sonora('click')" title="Avançar">
				<i class="fas fa-2x text-white fa-chevron-circle-right"></i>
			</a>

		</nav>